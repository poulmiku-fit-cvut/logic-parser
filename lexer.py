def parse_string(string):
    tokens = []
    values = {}

    for character in string:
        if character >= "A" and character <= "Z":
            values[character] = None
            tokens.append(("variable", character))
        elif character == "(":
            tokens.append(("bracket_start", None))
        elif character == ")":
            tokens.append(("bracket_end", None))
        elif character == "!":
            tokens.append(("negation", None))
        elif character == "&":
            tokens.append(("conjunction", None))
        elif character == "|":
            tokens.append(("disjunction", None))
        elif character == ">":
            tokens.append(("implication", None))
        elif character == "=":
            tokens.append(("equivalence", None))
        else:
            SyntaxError("incorrect syntax, unknown character: '%s'" % character)

    return tokens, values
