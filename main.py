import sys

from ast import *
from lexer import parse_string
from parser import Parser

print("and: &")
print("or: |")
print("implication: >")
print("equivalence: =")
print("negation: !")
print("variables: a-zA-Z")
print("brackets: ()")

expression_input = input("Insert logical statement to parse: ")

expression_input = expression_input.upper()

(tokens, values) = parse_string(expression_input)

parser = Parser(tk=tokens)

try:
    st = parser.parse_expressions()
    if isinstance(st, Brackets):
        tree = st
    else:
        tree = Brackets(st)
except SyntaxError as e:
    print(e)
    print("syntax error")
    sys.exit()

print(u"Statement: %s" % tree)

print("Tree:")
tree.tree(0)

while None in [values[letter] for letter in values]:
    for letter in values:
        if values[letter] is None:
            val = input("Enter the value for letter %s (0 or 1): " % letter)

            if val not in ["0", "1"]:
                pass
            else:
                values[letter] = True if val == "1" else False

evaluated = tree(values)

if evaluated:
    print("The value of the statement is 1 (true)")
else:
    print("The value of the statement is 0 (false)")

letters = [letter for letter in values]

trues = 0
falses = 0

for i in range(2 ** len(letters)):
    setup = str(bin(i)).replace("0b", "")[::-1]
    place = 0
    for s in setup:
        values[letters[place]] = True if s == "1" else False
        place += 1

    evaluated = tree(values)

    if evaluated:
        trues += 1
    else:
        falses += 1

if trues == 0:
    print("This statement is a contradiction, it's never true")
elif falses == 0:
    print("This statement is a tautology, it's always true")
else:
    print("This statement is sometimes true and sometimes false")
