# encoding=utf-8

__all__ = [
    "Brackets",
    "Variable",
    "BinaryOperator",
    "Conjunction",
    "Disjunction",
    "Implication",
    "Equivalence",
    "UnaryOperator",
    "Negation",
]

padding_text = "  "


class Brackets(object):

    def __init__(self, value):
        if not value:
            raise SyntaxError
        self.value = value

    def __str__(self):
        return "(%s)" % self.value

    def __unicode__(self):
        return u"(%s)" % self.value

    def __call__(self, values):
        return self.value(values)

    def tree(self, padding):
        print(padding_text * padding + "|--()")
        self.value.tree(padding + 1)


class Variable(object):

    def __init__(self, name):
        self.name = name

    def __str__(self):
        return "%s" % self.name

    def __unicode__(self):
        return u"%s" % self.name

    def __call__(self, values):
        return values[self.name]

    def tree(self, padding):
        print(padding_text * padding + "|--" + self.name)


class BinaryOperator(object):
    operator = None

    def __init__(self, left, right):
        self.left = left
        self.right = right

    def __str__(self):
        return "%s%s%s" % (self.left, self.operator, self.right)

    def __unicode__(self):
        return u"%s%s%s" % (self.left, self.operator, self.right)

    def tree(self, padding):
        print(padding_text * padding + "|--" + self.__class__.__name__)
        self.left.tree(padding + 1)
        self.right.tree(padding + 1)


class Conjunction(BinaryOperator):
    operator = "&"

    def __call__(self, values):
        return self.left(values) and self.right(values)


class Disjunction(BinaryOperator):
    operator = "|"

    def __call__(self, values):
        return self.left(values) or self.right(values)


class Implication(BinaryOperator):
    operator = ">"

    def __call__(self, values):
        val_right = self.right(values)
        val_left = self.left(values)

        return val_right or (not val_left and not val_right)


class Equivalence(BinaryOperator):
    operator = "="

    def __call__(self, values):
        val_right = self.right(values)
        val_left = self.left(values)

        return val_right == val_left


class UnaryOperator(object):
    operator = None

    def __init__(self, right):
        self.right = right

    def __str__(self):
        return "%s%s" % (self.operator, self.right)

    def __unicode__(self):
        return u"%s%s" % (self.operator, self.right)

    def tree(self, padding):
        print(padding_text * padding + "|--" + self.__class__.__name__)
        self.right.tree(padding + 1)


class Negation(UnaryOperator):
    operator = "!"

    def __call__(self, values):
        return not self.right(values)
