# encoding=utf-8

from ast import *


class Parser(object):
    def __init__(self, tk):
        self.tokens = tk

    def top(self):
        if not len(self.tokens):
            return None
        else:
            return self.tokens[0]

    def pop(self):
        if self.top():
            return self.tokens.pop(0)
        else:
            return None

    def parse_expressions(self):
        if not len(self.tokens):
            raise SyntaxError("Expected more tokens")

        left = self.parse_e1()

        while self.top() and self.top()[0] == "equivalence":
            self.pop()
            right = self.parse_e1()
            left = Equivalence(left, right)

        return left

    def parse_e1(self):
        left = self.parse_e2()
        while self.top() and self.top()[0] == "implication":
            self.pop()
            right = self.parse_e2()
            left = Implication(left, right)

        return left

    def parse_e2(self):
        left = self.parse_e3()
        while self.top() and self.top()[0] in ["conjunction", "disjunction"]:
            x = self.pop()
            right = self.parse_e3()
            if x[0] == "conjunction":
                left = Conjunction(left, right)
            else:
                left = Disjunction(left, right)

        return left

    def parse_e3(self):
        if self.top()[0] == "negation":
            self.pop()
            return Negation(self.parse_f())
        else:
            return self.parse_f()

    def parse_f(self):
        if self.top()[0] == "variable":
            tk = self.pop()
            letter = tk[1]
            return Variable(letter)
        elif self.top()[0] == "bracket_start":
            self.pop()
            x = self.parse_expressions()
            statement = Brackets(x)
            if self.top()[0] != "bracket_end":
                raise SyntaxError("Missing brackets at the end")
            else:
                self.pop()
            return statement
